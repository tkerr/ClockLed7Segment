# ClockLed7Segment #
The ClockLed7Segment class displays hours and minutes on a 7-segment LED display.

Designed for the Adafruit 7-segment LED FeatherWing display based on the HT16K33 
I2C matrix driver chip.

### Dependencies ###
- Arduino Wire library
- Adafruit_GFX_Library
- Adafruit_LED_Backpack

### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License  
https://opensource.org/licenses/MIT
