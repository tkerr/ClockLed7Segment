/******************************************************************************
 * SimpleTimer.ino
 * Copyright (c) 2019 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
  ******************************************************************************/

/**
 * @file
 * @brief
 * ClockLed7Segment example sketch. Implements a simple timer that increments
 * every second.
 *
 * The sketch uses the millis() function for timing, so it is not precicely accurate.
 * There is also no way to start, stop or reset the timer.
 *
 * Hardware:
 *   - Adafruit 7-segment LED FeatherWing display based on the HT16K33 I2C 
 *     matrix driver chip
 *
 * Libraries:
 *   - Arduino Wire library
 *   - Adafruit_GFX_Library
 *   - Adafruit_LED_Backpack
 */
 
#include <Arduino.h>
#include "ClockLed7Segment.h"

ClockLed7Segment display;
uint8_t minutes = 0;
uint8_t seconds = 0;
uint32_t last_millis = 0;


void setup()
{
    // Turn off the built-in Arduino LED.
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    
    // Initialize the 7-segment display.
    // Display dashes for 2 seconds just to demonstrate its use.
    display.init();
    display.displayDashes();
    delay(2000);
    
    // Show an animated spin just to demonstrate its use.
    for (int i = 0; i < 7; i++)
    {
        display.spin();
        delay(500);
    }
    
    // Initialize the display time to zero.
    display.displayTime(minutes, seconds, ClockLed7Segment::FORMAT_24HR);
    last_millis = millis();
}

void loop()
{
    uint32_t now = millis();
    if ((now - last_millis) >= 1000)
    {
        last_millis = now;
        seconds++;
        if (seconds > 59)
        {
            seconds = 0;
            minutes++;
            if (minutes > 59) minutes = 0;
        }
        display.displayTime(minutes, seconds, ClockLed7Segment::FORMAT_24HR);
    }
}
