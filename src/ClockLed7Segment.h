/******************************************************************************
 * ClockLed7Segment.h
 * Copyright (c) 2019 Thomas Kerr AB3GY (ab3gy@arrl.net)
 * Designed for personal use by the author, but available to anyone under the
 * license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * ClockLed7Segment class definition for the AB3GY internet clock project and
 * GPS clock project. 
 * Displays hours and minutes on a 7-segment LED display.
 *
 * Hardware:
 *   - Adafruit 7-segment LED FeatherWing display based on the HT16K33 I2C 
 *     matrix driver chip
 *
 * Libraries:
 *   - Arduino Wire library
 *   - Adafruit_GFX_Library
 *   - Adafruit_LED_Backpack
 */

#ifndef _CLOCK_LED_7_SEGMENT_H
#define _CLOCK_LED_7_SEGMENT_H

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdbool.h>
#include <stdint.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "Adafruit_LEDBackpack.h"


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
#define CLOCK_DISPLAY_MAX_BRIGHTNESS (15)


/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/

/**
 * @class
 * @brief Clock display class for various clock projects.
 *
 * Provides methods for displaying time, setup and error indications on a
 * 7-segment clock display.
 *
 * Designed for the Adafruit 7-segment LED FeatherWing display based on the 
 * HT16K33 I2C matrix driver chip.
 *
 * Uses the corresponding Adafruit GFX and LED backpack libraries.
 */
class ClockLed7Segment
{
public:

    /**
     * @brief Time display format specifier.
     */
    enum DisplayFormat
    {
        FORMAT_12HR = 0,  //!< 12-hour time format (AM/PM)
        FORMAT_24HR,      //!< 24-hour time format (00:00 - 23:59)
    };

    static const uint8_t DEFAULT_I2C_ADDRESS = 0x70;
    static const uint8_t DEFAULT_BRIGHTNESS  = CLOCK_DISPLAY_MAX_BRIGHTNESS;

    /**
     * @brief Default constructor.
     */
    ClockLed7Segment();

    /**
     * @brief Initialize ClockLed7Segment object and hardware.
     *
     * @param addr The I2C address of the display.  Set to default if not specified.
     *
     * @param brightness The initial display brightness.  Set to default if not specified.
     *
     * @return True if initialization is successful, false otherwise.
     */
    bool init(uint8_t addr = DEFAULT_I2C_ADDRESS, uint8_t brightness = DEFAULT_BRIGHTNESS);

    /**
     * @brief Blank the display (turn all segments and DPs off).
     */
    void blank();

    /**
     * @brief Display the time on the clock display.
     *
     * @param hours The hours to display (0-23)
     * @param minutes The minutes to display (0-59)
     * @param format The time display format (default = 12 hour)
     *
     * Note that minutes and seconds could be used in place of hours and minutes,
     * respectively.  
     */
    void displayTime(uint8_t hours, uint8_t minutes, DisplayFormat format = FORMAT_12HR);

    /**
     * @brief Display all dashes on the display (center segments lit).
     */
    void displayDashes();

    /**
     * @brief Display the word "prog" on the display.
     */
    void displayProg();

    /**
     * @brief Turn a decimal point on/off.
     *
     * @param pos The display position (0, 1, 3, 4)
     * @param on True turns decimal point on, false turns it off
     */
    void setDp(uint8_t pos, bool on);

    /**
     * @brief Set the display brightness.
     *
     * @param brightness The brightness setting (0 - 15)
     */
    void setBrightness(uint8_t brightness);

    /**
     * @brief Animate the display by "spinning" each segment in a circle.
     *
     * Each call advances the segment by one step.
     *
     * @param init Initialize the active segment to the 'a' segment if true (default = false).
     */
    void spin(bool init=false);

private:

    Adafruit_7segment m_display;  //!< The 7-segment display object

};


#endif // _CLOCK_LED_7_SEGMENT_H
