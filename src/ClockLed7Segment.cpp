/******************************************************************************
 * ClockLed7Segment.cpp
 * Copyright (c) 2019 Thomas Kerr AB3GY (ab3gy@arrl.net)
 * Designed for personal use by the author, but available to anyone under the
 * license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * ClockLed7Segment class implementation for the AB3GY internet clock project 
 * and GPS clock project. 
 * Displays hours and minutes on a 7-segment LED display.
 *
 * Hardware:
 *   - Adafruit 7-segment LED FeatherWing display based on the HT16K33 I2C 
 *     matrix driver chip
 *
 * Libraries:
 *   - Arduino Wire library
 *   - Adafruit_GFX_Library
 *   - Adafruit_LED_Backpack
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>
#include <Wire.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "Adafruit_GFX.h"
#include "ClockLed7Segment.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
 
 
/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/
 
/**************************************
 * ClockLed7Segment::ClockLed7Segment()
 **************************************/ 
ClockLed7Segment::ClockLed7Segment()
{
    m_display = Adafruit_7segment();
}


/**************************************
 * ClockLed7Segment::init()
 **************************************/ 
bool ClockLed7Segment::init(uint8_t addr, uint8_t brightness)
{
    m_display.begin(addr);
    blank();
    setBrightness(brightness);
    return true;
}


/**************************************
 * ClockLed7Segment::blank()
 **************************************/
void ClockLed7Segment::blank()
{
    m_display.displaybuffer[0] = 0x00;
    m_display.displaybuffer[1] = 0x00;
    m_display.displaybuffer[2] = 0x00;
    m_display.displaybuffer[3] = 0x00;
    m_display.displaybuffer[4] = 0x00;
    m_display.writeDisplay();
}


/**************************************
 * ClockLed7Segment::displayTime()
 **************************************/
void ClockLed7Segment::displayTime(uint8_t hours, uint8_t minutes, DisplayFormat format)
{
    int displayValue = (100 * hours) + minutes;
    
    // 12-hour AM/PM conversion.
    if (format == FORMAT_12HR)
    {
        if (hours > 12)
        {
            displayValue -= 1200;
        }
        else if (hours == 0)
        {
            displayValue += 1200;
        }
    }
    
    // Send the time value to the display.
    m_display.print(displayValue, DEC);
    
    // 24-hour formatting.
    // Need to add zero padding during the midnight (zero) hour.
    // NOTE: On the LED display, positions 0-1 are the hours, and 3-4 are the minutes.
    // Position 2 is the colon.
    if (format == FORMAT_24HR)
    {
        if (hours < 10) m_display.writeDigitNum(0, 0); // Tens of hours digit
        if (hours == 0)
        {
            m_display.writeDigitNum(1, 0);  // Hours digit
            if (minutes < 10) m_display.writeDigitNum(3, 0);  // Tens of minutes dight
        }
    }
    
    // Update the 7-segment display.
    m_display.writeDisplay();
    m_display.drawColon(true);
    m_display.writeColon();
}


/**************************************
 * ClockLed7Segment::displayDashes()
 **************************************/
void ClockLed7Segment::displayDashes()
{
    m_display.displaybuffer[0] = 0x40;
    m_display.displaybuffer[1] = 0x40;
    m_display.displaybuffer[2] = 0x00;
    m_display.displaybuffer[3] = 0x40;
    m_display.displaybuffer[4] = 0x40;
    m_display.writeDisplay();
}


/**************************************
 * ClockLed7Segment::displayProg()
 **************************************/
void ClockLed7Segment::displayProg()
{
    // Write "Prog" to the clock display.
    m_display.displaybuffer[0] = 0x73;
    m_display.displaybuffer[1] = 0x50;
    m_display.displaybuffer[2] = 0x00;
    m_display.displaybuffer[3] = 0x5C;
    m_display.displaybuffer[4] = 0x6F;
    m_display.writeDisplay();
}


/**************************************
 * ClockLed7Segment::setDp()
 **************************************/
void ClockLed7Segment::setDp(uint8_t pos, bool on)
{
    if ((pos == 2) || (pos > 4)) return;
    
    if (on)
    {
        m_display.displaybuffer[pos] |= 0x80;
    }
    else
    {
        m_display.displaybuffer[pos] &= 0x7F;
    }
    m_display.writeDisplay();
}


/**************************************
 * ClockLed7Segment::setBrightness()
 **************************************/
void ClockLed7Segment::setBrightness(uint8_t brightness)
{
    if (brightness > CLOCK_DISPLAY_MAX_BRIGHTNESS) brightness = CLOCK_DISPLAY_MAX_BRIGHTNESS;
    m_display.setBrightness(brightness);
}


/**************************************
 * ClockLed7Segment::spin()
 **************************************/
void ClockLed7Segment::spin(bool init)
{
    static uint16_t seg = 0x01;
    if (init) seg = 0x01;

    m_display.displaybuffer[0] = seg;
    m_display.displaybuffer[1] = seg;
    m_display.displaybuffer[2] = 0x00;
    m_display.displaybuffer[3] = seg;
    m_display.displaybuffer[4] = seg;
    m_display.writeDisplay();

    seg <<= 1;
    if (seg > 0x20) seg = 0x01;
}


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/

 
// End of file.
